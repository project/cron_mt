<?php

/**
 * @file
 * Contains all of the private functions used to run cron and fork the parent
 * process to run hook_cron() implementations in parallel.
 */

/**
 * Runs cron in all of it's pseudo multi-threaded glory.  This function locks
 * the core cron semaphore and utilizes some core cron functions.
 *
 * @return
 *   A boolean flagging whether cron ran successfully.
 */
function _cron_mt_run() {
  global $_cron_mt_processes;
  $_cron_mt_processes = array();

  // increase the maximum execution time if not is safe mode
  if (!ini_get('safe_mode')) {
    set_time_limit(240);
  }

  // fetch the cron semaphore, check if cron is still running
  $semaphore = variable_get('cron_semaphore', FALSE);
  if ($semaphore) {
    if (time() - $semaphore > 3600) {
      $error = 'Cron has been running for more than an hour and is most likely stuck.';
      watchdog('cron_mt', $error, array(), WATCHDOG_ERROR);
      variable_del('cron_semaphore');
    }
    else {
      $error = 'Attempting to re-run cron while it is already running.';
      watchdog('cron_mt', $error, array(), WATCHDOG_WARNING);
    }
  }
  else {

    // gets modules that implement hook_cron()
    $modules = array();
    foreach (module_list() as $module) {
      if (module_hook($module, 'cron')) {
        $modules[] = $module;
      }
    }

    // gets verbose mode
    $verbose = variable_get('cron_mt_verbose', 0);

    // sets start time, watchdog message that the cron run has started
    $cron_start = microtime(TRUE);
    watchdog('cron_mt', 'Cron run started.', array(), WATCHDOG_NOTICE);

    // cron has started, passes the parent's process ID to _cron_mt_cleanup()
    register_shutdown_function('_cron_mt_cleanup', posix_getpid());
    variable_set('cron_semaphore', time());

    // invoke hook_cron() implementations, fork processes as necessary
    $quit = FALSE;
    $error = FALSE;
    while (!$quit) {

      // attempts to fork a new process, pops module off of stack
      if (FALSE !== ($pid = _cron_mt_new_process())) {
        $module = array_shift($modules);
        if (0 == $pid) { // child process
          if (count($modules)) {

            // re-connects to database, gets next hook_cron() implementation
            db_set_active('default', TRUE);
            $hook_cron = sprintf('%s_cron', $module);

            // if verbose mode, sets start time and watchdog message
            if ($verbose) {
              $hook_start = microtime(TRUE);
              $variables = array('%hook' => sprintf('%s()', $hook_cron));
              watchdog('cron_mt', 'Invoking %hook.', $variables, WATCHDOG_NOTICE);
            }

            // invokes hook_cron() implementation
            $hook_cron();

            // if verbose mode, sets end time and watchdog message
            if ($verbose) {
              $hook_end = microtime(TRUE);
              $hook_time = round(($hook_end - $hook_start), 2);
              if (!$hook_time) {
                $hook_time = '< 0.01';
              }
              $variables['%time'] = $hook_time;
              $message = '%hook run complete in %time seconds.';
              watchdog('cron_mt', $message, $variables, WATCHDOG_NOTICE);
            }
          }

          // ends the child process
          exit(0);
        }
        elseif (-1 == $pid) { // error
          $quit = TRUE;
          $error = TRUE;
          watchdog('cron_mt', 'Error forking process.', array(), WATCHDOG_ERROR);
        }
      }

      // quit loop if no modules are left
      if (empty($modules)) {
        $quit = TRUE;
      }

      // cleans up list of child processes, pauses execution
      _cron_mt_cleanup_processes(TRUE);
      usleep(100000);
    }

    // waits for all children to exit, re-connects to database
    _cron_mt_cleanup_processes();
    db_set_active('default', TRUE);

    // calculates the time it took to run cron, sets t() variables
    $variables = array();
    $cron_time = round((microtime(TRUE) - $cron_start), 2);
    if (!$cron_time) {
      $cron_time = '< 0.01';
    }
    $variables['%time'] = $cron_time;

    // sets watchdog message that cron run is complete
    $message = 'Cron run completed in %time seconds.';
    watchdog('cron_mt', $message, $variables, WATCHDOG_NOTICE);

    // records time cron finished, deletes semaphore
    variable_set('cron_last', time());
    variable_del('cron_semaphore');

    // Return TRUE so other functions can check if it did run successfully
    return !$error;
  }
}

/**
 * Cleans up cron process.  This will be called by every child process that
 * exits, so we need to call drupal_cron_cleanup() only when _cron_mt_cleanup()
 * is called by the parent process.
 *
 * @param int
 *   An integer containing the parent process ID.
 * @return
 *   NULL
 * @see drupal_cron_cleanup()
 */
function _cron_mt_cleanup($pid) {
  if (posix_getpid() == $pid) {
    drupal_cron_cleanup();
  }
}

/**
 * Checks whether or not we can fork another process by checking current number
 * of processes against the process limit.
 *
 * @return
 *   A boolean flagging whether we can fork another process.
 */
function _cron_mt_process_available() {
  global $_cron_mt_processes;
  $process_limit = variable_get('cron_mt_process_limit', 2);
  return (count($_cron_mt_processes) < $process_limit);
}

/**
 * Forks a new process if we are within the limit.
 *
 * @param $force
 *   A boolean flagging whether a new process should be forked regardless of
 *   whether or not we are within the limit.
 * @return
 *   An iteger containing the process ID, FALSE on failure.
 */
function _cron_mt_new_process($force = FALSE) {
  global $_cron_mt_processes;
  if (_cron_mt_process_available() || $force) {
    $pid = pcntl_fork();
    if ($pid > 0) {
      $_cron_mt_processes[] = $pid;
    }
    return $pid;
  }
  else {
    return FALSE;
  }
}

/**
 * Check processes, purges those that are no longer running.
 *
 * @param $nohang
 *   A boolean flagging whether to pass the WNOHANG flag to pcntl_waitpid().
 * @return
 *   NULL
 */
function _cron_mt_cleanup_processes($nohang = FALSE) {
  global $_cron_mt_processes;
  $options = (!$nohang) ? NULL : WNOHANG;
  foreach ($_cron_mt_processes as $key => $cur_pid) {
    $pid = pcntl_waitpid($cur_pid, $status, $options);
    if ($pid > 0) {
      unset($_cron_mt_processes[$key]);
    }
  }
}
