<?php

/**
 * @file
 * Administrative settings callbacks for Cron MT.
 */

/**
 * Administrative settings.
 *
 * @return
 *   An array of form elements.
 */
function cron_mt_admin_settings() {
  $form = array();

  // description for the process limit
  $description = 'Limits the number of processes that may be forked at one'
               .' time. This number should be set to at least the number of'
               .' CPUs on the system.  For example, a quad-core processor can'
               .' handle 4 processes in parallel.';

  // result set limitation to prevent excessive score calculations
  $form['cron_mt_process_limit'] = array(
    '#type' => 'select',
    '#title' => t('Process limit'),
    '#default_value' => variable_get('cron_mt_process_limit', 2),
    '#options' => drupal_map_assoc(range(1, 32)),
    '#description' => t($description),
  );

  // flag whether to run in verbose mode
  $form['cron_mt_verbose'] = array(
    '#type' => 'radios',
    '#title' => t('Verbose mode'),
    '#default_value' => variable_get('cron_mt_verbose', 0),
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
    '#description' => t(
      'Log start and stop times for individual hook_cron() implementations.'
    ),
  );

  return system_settings_form($form);
}
