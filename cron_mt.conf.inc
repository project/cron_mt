<?php

/**
 * @file
 * Configuration file parser function.
 */

/**
 * Parses the configuration file.
 *
 * @param $errstr
 *   Retruns the error message.
 * @return
 *   An array containing the configuration directives, FALSE on errors.
 */
function _cron_mt_parse_conf_file(&$errstr = NULL) {
  $errstr = '';

  // gets the path to the conf file, parses conf file
  $conf_file = sprintf('%s/etc/cron_mt.conf', dirname(__FILE__));
  if ($conf = @parse_ini_file($conf_file, TRUE)) {
    if (array_key_exists('drupal', $conf)) {
      if (array_key_exists('path', $conf['drupal'])) {
        $bootstrap_file = sprintf('%s/includes/bootstrap.inc', $conf['drupal']['path']);
        if (is_file($bootstrap_file)) {
          require_once $bootstrap_file;
          if (defined('DRUPAL_BOOTSTRAP_FULL')) {
            chdir($conf['drupal']['path']);
            $conf['drupal']['bootstrap'] = $bootstrap_file;
          }
          else {
            $errstr = 'The bootstrap.inc file is corrupt, expected constants not found.';
          }
        }
        else {
          $errstr = 'The bootstrap.inc file not found: check drupal root.';
        }
      }
      else {
        $errstr = 'The path directive is required in the drupal section of the configuration file.';
      }
    }
    else {
      $errstr = 'The drupal section is required in the configuration file.';
    }
  }
  else {
    if (!is_file($conf_file)) {
      $errstr = 'Configuration file not found';
    }
    else {
      $errstr = 'Error parsing configuration file';
    }
  }

  // if there are no error messages, returns configuration directives
  return (empty($errstr)) ? $conf : FALSE;
}
