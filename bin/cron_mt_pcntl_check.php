#!/usr/bin/env php
<?php

/**
 * @file
 * Tests whether the PCNTL extension is installed.  This script is intended to
 * be run via command line interface, because the extension will never be loaded
 * in a non-CLI environment.
 */

// tests if the process control extension is loaded
$exit_code = (int)!extension_loaded('pcntl');
exit($exit_code);
