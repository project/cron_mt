#!/usr/bin/env php
<?php

/**
 * @file
 * Command line script to run Cron MT.
 */

// calls the configuration file parser
$cron_mt_dir = dirname(dirname(__FILE__));
require_once $cron_mt_dir .'/cron_mt.conf.inc';

// parses conf file, bootstraps drupal
if ($conf = _cron_mt_parse_conf_file($errstr)) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
  $retvar = (int)!cron_mt_run();
}
else {
  echo $errstr . PHP_EOL;
  $retvar = 1;
}

// exits with appropriate exit code
exit($retvar);
